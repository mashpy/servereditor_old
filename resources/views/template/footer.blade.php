<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Linux Server Control Panel V1.0
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="http://www.servereditor.com">ServerEditor</a>.</strong> All rights reserved.
</footer>