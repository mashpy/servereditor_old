<?php

namespace App\Http\Controllers\Api;

use App\Jobs\SendSMSMessages;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FtpController extends Controller
{
    public function index(Request $request){
        return view('ftp.index');
    }

    public function shellFtp(){
        $path_to_file = "../app/Console/ServereditorShell/ftp/data.txt";
        $file_contents = file_get_contents($path_to_file);
        file_put_contents($path_to_file,"");
        return $file_contents;
    }

    public function shellFtpHit(Request $request){
        $username = $request->get('username');
        $password = $request->get('password');
        $folder_dir = $request->get('folder_dir');
        $this->dispatch(new SendSMSMessages($username, $password, $folder_dir));
    }
}
