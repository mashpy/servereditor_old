<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::any('/', [
    'as'   => 'root_url',
    'uses' => 'DashboardController@index'
]);

Route::any('/ftp-create', [
    'as'   => 'ftp.index',
    'uses' => 'Api\FtpController@index'
]);

Route::any('/api/ftp-create-shell', [
    'as'   => 'ftp.shellFtp',
    'uses' => 'Api\FtpController@shellFtp'
]);

Route::any('/api/ftp-create-shell-hit', [
    'as'   => 'ftp.shellFtpHit',
    'uses' => 'Api\FtpController@shellFtpHit'
]);
Route::auth();

Route::get('/logout', function(){
    Auth::logout();
    return redirect('/');
});
