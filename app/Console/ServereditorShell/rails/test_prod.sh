app="testapp"
cd ~
cd $app/config
sed -i -e 's/<%= ENV\[\"SECRET_KEY_BASE\"\] %>/development/g' secrets.yml
wget -N https://bitbucket.org/mashpy/server-editor-installer/raw/21da6cb12219b273b45831b75533dc51cf5ffdb8/rails/config_files/puma.rb
cd ..
mkdir -p shared/pids shared/sockets shared/log
cd /etc/init
sudo wget -N https://bitbucket.org/mashpy/server-editor-installer/raw/21da6cb12219b273b45831b75533dc51cf5ffdb8/rails/config_files/puma.conf
sudo wget -N https://bitbucket.org/mashpy/server-editor-installer/raw/21da6cb12219b273b45831b75533dc51cf5ffdb8/rails/config_files/puma-manager.conf
cd ..
sudo wget -N https://bitbucket.org/mashpy/server-editor-installer/raw/21da6cb12219b273b45831b75533dc51cf5ffdb8/rails/config_files/puma2.conf
sudo mv puma2.conf puma.conf
sudo stop puma-manager
sudo start puma-manager
sudo apt-get -y install nginx
cd /etc/nginx/sites-available/
rm -rf default
sudo wget -N https://bitbucket.org/mashpy/server-editor-installer/raw/21da6cb12219b273b45831b75533dc51cf5ffdb8/rails/config_files/default
sudo service nginx restart
sudo stop puma-manager
sudo start puma-manager
sudo service nginx restart
