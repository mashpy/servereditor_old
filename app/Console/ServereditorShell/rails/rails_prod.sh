cd
current_user=${PWD##*/}

echo "software is updating......................................
................................................................"
sudo apt-get update

echo "Installing essential software ..................................
....................................................................."
sudo apt-get -y install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev libmagickwand-dev


gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
curl -sSL https://get.rvm.io | bash -s stable --rails

cd ~
source /home/$current_user/.rvm/scripts/rvm

rvm install 2.2

rvm use 2.2

ruby -v
echo "gem: --no-document" > ~/.gemrc
gem install bundler
gem install rails

cd ~

rails -v
#sudo add-apt-repository ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get -y install nodejs bundler
sudo apt-get -y install mysql-server mysql-client libmysqlclient-dev
gem install mysql2
gem install bundler
cd ~

app="testapp"
rails new $app
cd $app
source /home/$current_user/.rvm/scripts/rvm
rvm use 2.2
bundle install
cd ~
cd $app/config
wget -N https://bitbucket.org/mashpy/server-editor-installer/raw/f456643ea6507d48ad2688370d93acb39a7a2511/rails/config_files/puma.rb
sed -i -e 's/<%= ENV\[\"SECRET_KEY_BASE\"\] %>/development/g' secrets.yml
cd ..
mkdir -p shared/pids shared/sockets shared/log
cd /etc/init
sudo wget -N https://bitbucket.org/mashpy/server-editor-installer/raw/f456643ea6507d48ad2688370d93acb39a7a2511/rails/config_files/puma.conf
sudo sed -i -e "s/servereditor_user/$current_user/g" puma.conf
sudo wget -N https://bitbucket.org/mashpy/server-editor-installer/raw/f456643ea6507d48ad2688370d93acb39a7a2511/rails/config_files/puma-manager.conf
cd ..
sudo wget -N https://bitbucket.org/mashpy/server-editor-installer/raw/f456643ea6507d48ad2688370d93acb39a7a2511/rails/config_files/puma2.conf
sudo sed -i -e "s/servereditor_user/$current_user/g" puma2.conf
sudo sed -i -e "s/servereditor_app/$app/g" puma2.conf
sudo mv puma2.conf puma.conf
sudo chown $current_user:$current_user -R /home/$current_user/$app
sudo stop puma-manager
sudo start puma-manager
sudo apt-get -y install nginx
cd /etc/nginx/sites-available/
sudo wget -N https://bitbucket.org/mashpy/server-editor-installer/raw/df19d236637767f7a792a1fd87119ea0e5630e89/rails/config_files/default
sudo sed -i -e "s/servereditor_user/$current_user/g" default
sudo sed -i -e "s/servereditor_app/$app/g" default
sudo service nginx restart
sudo stop puma-manager
sudo start puma-manager
sudo service nginx restart
