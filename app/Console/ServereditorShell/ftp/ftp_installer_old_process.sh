username=$1
password=$2
home_dir=/home/$1
upload_folder=$home_dir/$3

sudo apt-get update
sudo apt-get install vsftpd
cd /etc
sudo wget -N https://bitbucket.org/mashpy/server-editor-installer/raw/55ed892e43d3e8fa12d681fb3935bfa220a03235/ftp/config/vsftpd.conf
sudo service vsftpd restart
#echo -n admin123 | makepasswd --crypt-md5 --clearfrom -
sudo adduser --disabled-password --gecos "" $username
echo -e "$password\n$password\n" | sudo passwd $username
sudo chown root:root $home_dir
sudo mkdir $upload_folder
sudo chown $username:$username $upload_folder
sudo service vsftpd restart
