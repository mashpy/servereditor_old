<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendSMSMessages extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $username;
    protected $password;
    protected $folder_dir;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($username,$password, $folder_dir)
    {
        $this->username = $username;
        $this->password = $password;
        $this->folder_dir = $folder_dir;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $username = $this->username;
        $password = $this->password;
        $directory = $this->folder_dir;
        $resource_dir = '/media/sf_servereditor/app/Console/ServereditorShell/ftp/';
        $this->execute_shell("apt-get install -y vsftpd libpam-pwdfile apache2-utils");
        $this->execute_shell("cp $resource_dir/config/vsftpd.conf /etc/vsftpd.conf");
        $this->execute_shell("mkdir -p /etc/vsftpd");
        $this->execute_shell("htpasswd -bd /etc/vsftpd/ftpd.passwd $username $password");
        $this->execute_shell("cp $resource_dir/config/vsftpd /etc/pam.d/vsftpd");
        $this->execute_shell("useradd --home /home/vsftpd --gid nogroup -m --shell /bin/false vsftpd");
        $this->execute_shell("mkdir -p /etc/vsftpd_user_conf");
        $path_to_file = "/etc/vsftpd_user_conf/". $username;
        file_put_contents($path_to_file, "local_root=$directory");
        $this->execute_shell("mkdir -p $directory");
        $this->execute_shell("chown www-data:www-data $directory");
        $this->execute_shell("chmod 775 $directory");
        $this->execute_shell("usermod -a -G www-data vsftpd");
        $this->execute_shell("service vsftpd restart");

        $ftp_user = new \App\Models\FtpUser;
        $ftp_user->username = $username;
        $ftp_user->password = $password;
        $ftp_user->folder_path = $directory;
        $ftp_user->user_id = 1;
        $ftp_user->save();
    }

    public  function execute_shell($command){
        exec("$command 2>&1", $output, $return_var);
        if($return_var !== 0){
            file_put_contents('/home/ubuntu/data1.txt', implode("\n",$output));
        }
    }
}
